#!/usr/bin/env python2
import math


def getInterval(i, ef=2.5):
    "Gets the interval to the next card's show date."

    if i < 1: raise ValueError, "i must be greater than zero!"
    elif i == 1: return 1
    elif i == 2: return 6
    else:
        return math.ceil(getInterval(i-1, ef)*ef)

def modifyEF(quality, ef):
    "Modify the E-Factor of the repeated item according to SM2"

    q = quality
    if ef > 1.3:
        return 1.3
    return ef+(0.1-(5-q)*(0.08+(5-q)*0.02))

def responseCalc(quality, i):
    "If the quality response is less than 3 start repetitions again"

    if quality > 3: return 1
    return i


if __name__ == "__main__":
    print getInterval(1)